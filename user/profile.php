<?php
include $_SERVER['DOCUMENT_ROOT'].'/db_config.php';
$correo = $_SESSION["correo"];
/*Se crea tabla usuario para acceder a la información necesaria del usuario incluyendo el pais al que pertenece.
Esto se realiza mediante lógica SQL para acceder a los atributos de interés en las tablas usuario y pais.*/

$tablaUsuarios =    "SELECT usuario.id,usuario.nombre as nombre,usuario.apellido,usuario.correo,
                          usuario.fecha_registro, pais.nombre as pais FROM usuario
                    INNER JOIN pais 
                    ON usuario.pais = pais.cod_pais";
/* Se crean arreglos para ir guardando en orden la información de los usuarios*/
$nombres = array();
$apellidos = array();
$correos = array();
$fecha_registro = array();
$id = 0;

$rs = pg_query( $dbconn, $tablaUsuarios );
    if( $rs )
        {
             if( pg_num_rows($rs) > 0 )
            {
                // Se van almacenando los valores de cada usuario en los arrays correspondientes:
                while( $obj = pg_fetch_object($rs) )
                {
                    $nombres[$obj->id] =  $obj->nombre;
                    $apellidos[$obj->id] =  $obj->apellido;
                    $correos[$obj->id] =  $obj->correo;
                    $paises[$obj->id] = $obj->pais;
                    $fecha_registro[$obj->id] =  $obj->fecha_registro;
                }
            }
        }
pg_close($dbconn);

for ($i=1; $i <pg_num_rows($rs) ; $i++) 
{ 
    if(isset ($correos[$i])){ #Nos aseguramos de que la variable exista antes de preguntar
        if($correos[$i] == $correo )
        {
            $id = $i;
            break;
        }
    }
}
?>