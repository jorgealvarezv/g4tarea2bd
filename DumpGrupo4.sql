PGDMP     (                     y            Tarea1    13.3    13.3     �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16394    Tarea1    DATABASE     d   CREATE DATABASE "Tarea1" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Spanish_Chile.1252';
    DROP DATABASE "Tarea1";
                postgres    false            �            1259    16395    cuenta_bancaria    TABLE     �   CREATE TABLE public.cuenta_bancaria (
    numero_cuenta integer NOT NULL,
    id_usuario integer NOT NULL,
    balance numeric NOT NULL
);
 #   DROP TABLE public.cuenta_bancaria;
       public         heap    postgres    false            �            1259    16401    moneda    TABLE     �   CREATE TABLE public.moneda (
    id integer NOT NULL,
    sigla character varying(10) NOT NULL,
    nombre character varying(80) NOT NULL
);
    DROP TABLE public.moneda;
       public         heap    postgres    false            �            1259    16404    pais    TABLE     g   CREATE TABLE public.pais (
    cod_pais integer NOT NULL,
    nombre character varying(45) NOT NULL
);
    DROP TABLE public.pais;
       public         heap    postgres    false            �            1259    16407    precio_moneda    TABLE     �   CREATE TABLE public.precio_moneda (
    id_moneda integer NOT NULL,
    fecha timestamp without time zone NOT NULL,
    valor numeric NOT NULL
);
 !   DROP TABLE public.precio_moneda;
       public         heap    postgres    false            �            1259    16413    usuario    TABLE     L  CREATE TABLE public.usuario (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    apellido character varying(50),
    correo character varying(50) NOT NULL,
    "contraseña" character varying(50) NOT NULL,
    pais integer NOT NULL,
    fecha_registro timestamp without time zone NOT NULL,
    admin integer
);
    DROP TABLE public.usuario;
       public         heap    postgres    false            �            1259    16416    usuario_tiene_moneda    TABLE     �   CREATE TABLE public.usuario_tiene_moneda (
    id_usuario integer NOT NULL,
    id_moneda integer NOT NULL,
    balance numeric NOT NULL
);
 (   DROP TABLE public.usuario_tiene_moneda;
       public         heap    postgres    false            �          0    16395    cuenta_bancaria 
   TABLE DATA           M   COPY public.cuenta_bancaria (numero_cuenta, id_usuario, balance) FROM stdin;
    public          postgres    false    200   :        �          0    16401    moneda 
   TABLE DATA           3   COPY public.moneda (id, sigla, nombre) FROM stdin;
    public          postgres    false    201   �!       �          0    16404    pais 
   TABLE DATA           0   COPY public.pais (cod_pais, nombre) FROM stdin;
    public          postgres    false    202   -"       �          0    16407    precio_moneda 
   TABLE DATA           @   COPY public.precio_moneda (id_moneda, fecha, valor) FROM stdin;
    public          postgres    false    203   �"       �          0    16413    usuario 
   TABLE DATA           k   COPY public.usuario (id, nombre, apellido, correo, "contraseña", pais, fecha_registro, admin) FROM stdin;
    public          postgres    false    204    )       �          0    16416    usuario_tiene_moneda 
   TABLE DATA           N   COPY public.usuario_tiene_moneda (id_usuario, id_moneda, balance) FROM stdin;
    public          postgres    false    205   �,       8           2606    16423 $   cuenta_bancaria cuenta_bancaria_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.cuenta_bancaria
    ADD CONSTRAINT cuenta_bancaria_pkey PRIMARY KEY (numero_cuenta);
 N   ALTER TABLE ONLY public.cuenta_bancaria DROP CONSTRAINT cuenta_bancaria_pkey;
       public            postgres    false    200            :           2606    16425    moneda moneda_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.moneda
    ADD CONSTRAINT moneda_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.moneda DROP CONSTRAINT moneda_pkey;
       public            postgres    false    201            <           2606    16427    pais pais_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.pais
    ADD CONSTRAINT pais_pkey PRIMARY KEY (cod_pais);
 8   ALTER TABLE ONLY public.pais DROP CONSTRAINT pais_pkey;
       public            postgres    false    202            >           2606    16429     precio_moneda precio_moneda_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.precio_moneda
    ADD CONSTRAINT precio_moneda_pkey PRIMARY KEY (id_moneda, fecha);
 J   ALTER TABLE ONLY public.precio_moneda DROP CONSTRAINT precio_moneda_pkey;
       public            postgres    false    203    203            @           2606    16431    usuario usuario_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.usuario DROP CONSTRAINT usuario_pkey;
       public            postgres    false    204            B           2606    16433 .   usuario_tiene_moneda usuario_tiene_moneda_pkey 
   CONSTRAINT        ALTER TABLE ONLY public.usuario_tiene_moneda
    ADD CONSTRAINT usuario_tiene_moneda_pkey PRIMARY KEY (id_usuario, id_moneda);
 X   ALTER TABLE ONLY public.usuario_tiene_moneda DROP CONSTRAINT usuario_tiene_moneda_pkey;
       public            postgres    false    205    205            C           2606    16434 /   cuenta_bancaria cuenta_bancaria_id_usuario_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cuenta_bancaria
    ADD CONSTRAINT cuenta_bancaria_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.usuario(id);
 Y   ALTER TABLE ONLY public.cuenta_bancaria DROP CONSTRAINT cuenta_bancaria_id_usuario_fkey;
       public          postgres    false    200    204    2880            D           2606    16439 *   precio_moneda precio_moneda_id_moneda_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.precio_moneda
    ADD CONSTRAINT precio_moneda_id_moneda_fkey FOREIGN KEY (id_moneda) REFERENCES public.moneda(id);
 T   ALTER TABLE ONLY public.precio_moneda DROP CONSTRAINT precio_moneda_id_moneda_fkey;
       public          postgres    false    201    2874    203            E           2606    16444    usuario usuario_pais_fkey    FK CONSTRAINT     z   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pais_fkey FOREIGN KEY (pais) REFERENCES public.pais(cod_pais);
 C   ALTER TABLE ONLY public.usuario DROP CONSTRAINT usuario_pais_fkey;
       public          postgres    false    2876    202    204            F           2606    16449 8   usuario_tiene_moneda usuario_tiene_moneda_id_moneda_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuario_tiene_moneda
    ADD CONSTRAINT usuario_tiene_moneda_id_moneda_fkey FOREIGN KEY (id_moneda) REFERENCES public.moneda(id);
 b   ALTER TABLE ONLY public.usuario_tiene_moneda DROP CONSTRAINT usuario_tiene_moneda_id_moneda_fkey;
       public          postgres    false    2874    205    201            G           2606    16454 9   usuario_tiene_moneda usuario_tiene_moneda_id_usuario_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuario_tiene_moneda
    ADD CONSTRAINT usuario_tiene_moneda_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.usuario(id);
 c   ALTER TABLE ONLY public.usuario_tiene_moneda DROP CONSTRAINT usuario_tiene_moneda_id_usuario_fkey;
       public          postgres    false    204    2880    205            �   B  x����@C�P����_ǚH9�`l����)���3v���K�׺�����w����:�e��P�N���;;R�y������-��
h��5V�&����8Ycw`&�<@�f
i�M�Y1���q[u*��RʺG>5�����;Of���Ro�9O��-�V�[Gc�!U@$�lw+68i�m��{R6�`�>KI��db���2�ņ]	�V+%;H����UVW
}����W���(P��{��`�	��N�>�wb_B�08V�xG�Q�&�#�'��0�=*�~��u'�8J��܀���l��QBD!�G{���'��^2gH      �   �   x�-��
�@���>�O�[g��N`�F�b:#z}��ڝ��#�
��:8O���T�<���f6�^����B���{<
ma���fM��B�m�!2S�~�#�<C���e:!Q�?�:C��l�Q\�²��T�D��2�kED!�2�      �   t   x���1���p��Ox�. �DF��V�l�>'tC-4�F��`��U�p�����Y�!��KS�0�EBi���=�[S�coK���#.9O�Fq!&7��3��Y�6׏�"�i�#E      �   ?  x�]XK�#1[ON�$e��2�?�H8�Te�T�����Ѣ�S�G�[����-M��?���|��>��2���"7dI����N�K��"=��?Ey���2���"�����pP��Ճ����.�*�8��=��O�!��6n��������^m]�������׎�
���U��].R�Y�3���	Z

�A冴���!�5�J�t�E�Q�RxQ�G�q� %o-W�9EC%Ad��Z��-�}����z�E���9J�"���:���U�ԛ���UQ�߳���!�_��u��i��H�*��t�.h�t����6�4�ySPZX?-�Ӳ˼�5�6�9��O�i1��٢�]T�+C��𓨨H��4�_yJ@�gH�ou�Y	��x��].����#u�F�j(����M�Jx�����>z4f:e����D��Bk�� �����@��y)��P��j�~���3
�":�J�Tw�7��l�뢥�3s5A�K�ꀓ\/(n&�y�O�Q����K� Qʻ�I䎗�ڭ��"s ,꺤`��@Ћ���[�5��ʢ[#�V4��r��d@Թ����X��)L�����)�Y�w.@�!&ntMTwQ��׫��@c������!j0�W�_���v�.�P����TF�7�%�d�<#����>�j���%5(. �����<qQ��Nj��C!���"j%q"�K��/R��C<d�9i)nֺr��▙u�T�N�߄�Ls��}�{ �QȄ���v�{�p�"���)7/��xJ^���vzdO�I�Uw���pE,�499"2��N��~�+� �k��* �"�mBt��TаQČ�x�Q�����%<DY�bR`��x���AN� p�N�cgN��4!�3�Cf�!3m&�,�I�f9�;ȷb��������$$"n��G4Y-�[J�v���Wm@����� G*�w.���b<�!Mr�gMλ��R�H����-'Qع���o�$\KZ��r�/���-��Яf����1����i�þ=3d���fr�j�,�Rc��[Z�X������i��A�3��Q�����f��Ѳ���{�d�U����C(�v��E'ȶ�%���6wC�%��3[{O�?d�Ӻ���"�m#�%��:BL��-L]	�����Eq^@Բ�q��*�8�ʌ�E�g+��&��)6�\-��DM�m�i&���	�������Sˤ��C:���c���
���8�>S�5����bm��|{UI���L@����;0&��q|֭E=}@�� ���˫�����!�+.71����~t�,�m�,�n���؎<�l�KJ��b9��"s��V��G^�Baf�-��@���v�"Fm�G2�͵�1f��ۇ�;��1�4Փ��:��9�Cl��S��P����zaLؚ1��,O�����=X�b�یu�r�)� ����/�E��؃�N���ަ[�ge#���OHN���0&=�0g�H�uT��`����19�;?��+�=�5vsm̙�;��c�|��~�t�0���Y���;�����+�����{�{08�c�K��o��㺗��6R�}A�RjQ���z����}�      �   �  x�]�ߎ�6ƯO��/�e��^���N�]�\��7q�w�%LG��׆4��"t~H|:>��ڎa�³�ⳝ]�~j��w���\V��1M%0�����_0�u��Qx��h{�m��^g@?���ìec�+՘�7@p+3,3B�b9�����S���t����u���v�0K�((�E�+RC�E��,_)�]/� zxspu����w��hL�\����՜���)�**������4�pH��;��)�������L����VlƆw�:{�?l����8�;����˵_�LI�1fJI(��Oz".tl]������ z�q��\��T�T\`�蕩?J�z^�!���s��D�X>�*�ɵ����rJ�T�\��P�[o�m��=��Bk{(BKEK�TM�]����բm��>��/߾^�K5�H�f������F�x��g�m�H�	a� ���c&t�i峺fT�"�k�\�a��ͼ�?��G��C,QH妳��7�6TQ	�a6�7R9�����{�F��q��8g�H�qQ-��ir[w�9h���-|w�xMۅИh��9/���y.��!K�#[9�?.�z�6�i7zK�p�)E)�	o��~R����%clL��	��Q{��*��&���R�\����/�b�ɦ���j��v��JH�H}��*�rR(e�v%����.{������<���:$�l���Ε*����wl��p"%7����!��~��Rj����Ĩ{$�$F1<����q��}�����h\^�c��<��I%^Dc��S|R���#T��^��R�9�`e���y�q�Ȩ���)�������c� ի���W\�	^ʇm�O���q�?�!vw��5�8��,#뜋���U�X��鹱-�	�&�#�X�S*7V/+���x�)Rn�XLb���t�!��1U���`�TDp��ǡ1R�2��n�NH�      �   �   x���DAB���][s�������-5Ga����MwI�.}+	��:�yb��QOy+��4(�B[l��1T�t&e�w��2��q������iJ2ʵ+�׊��N}z�˝�-v�9Ol�bmWY���Sl/�(nܼ���{zܕ+��WLk�"���.�     