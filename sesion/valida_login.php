<?php 
include $_SERVER['DOCUMENT_ROOT'].'/db_config.php';
/* Este archivo manejar la lógica de iniciar sesión */
#Se reciben las credenciales ingresadas por el usuario
$correo =  $_POST["correo"];
$contraseña =  $_POST["pass"];

session_start();
if($_SERVER["REQUEST_METHOD"] == "POST")
{
   $_SESSION["correo"]= $correo;
   $_SESSION["pass"] = $contraseña;
   # Se guardan las credenciales de todos los usuarios en arrays
   $tablaUsuarios = "SELECT usuario.id ,usuario.contraseña, usuario.correo, usuario.admin FROM usuario";
   $correos = array();
   $contraseñas = array();
   $rs = pg_query( $dbconn, $tablaUsuarios );
      if( $rs )
         {
               if( pg_num_rows($rs) > 0 )
               {
                  // Recorrer el resource y mostrar los datos:
                  while( $obj = pg_fetch_object($rs) )
                  {
                     $correos[$obj->id] =  $obj->correo;
                     $contraseñas[$obj->id] = $obj->contraseña;
                     $roles[$obj->id] =  $obj->admin;
                     $id[$obj->id] =  $obj->id;
                  }
               }
         }
   pg_close($dbconn);
   $sesion = 0;
   #Revisa si las credenciales son validas
   for ($i=1; $i <(pg_num_rows($rs)+1) ; $i++) 
   { 
      if ($correos[$i] == $correo && $contraseñas[$i] ==$contraseña ) 
      {  
         $_SESSION['rol']=$roles[$i];
         $_SESSION['id']=$id[$i];
         
         $sesion = 1;
         break;
      }
   }
   #Si las credenciales son validas, se redirige al perfil del usuario con sesion iniciada
   if ($sesion == 1)
   {
      $sesion = 0;
      header('Location:/../user/profile.html');
      
   }
   else
   #Si las credenciales son invalidas se redirige a la pagina principal.
   {
      session_destroy();
      header('Location:/../index.html');
   }
   exit();
}
?>
