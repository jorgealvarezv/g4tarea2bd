Ramo:           -INF 239
Profesor:       -Cecilia Reyes
integrantes:			-Jorge Álvarez  			Rol:	201721065-6
                        -Juan Álvarez   			Rol:	201773016-1
                        -Cristian	Navarro			Rol:	201721036-2
Grupo 04.
Asunto: Tarea 2

Dentro de esta tarea se encuentran los archivos correspondientes que permiten la correcta ejecución de una página web

Supuestos y Consideraciones:

Para el desarrollo de esta página web se trabajo con los archivos de la planilla entregados para el desarrollo de esta, el único archivo que no se utilizo fue el "valida_Sesion.php" puesto que la lógica para comprobar si existe una sesión válida se fue incorporando en cada página de interés.
Se agregaron 3 imágenes (inicio_sesion.jpg, monedas.jpg y registro.jpg) en la carpeta img para mostrarse en la página principal junto a una imagen de fondo para añadir en las distintas ventanas con nombre fondo.jpg.

Se supuso que los usuarios administradores eran elegidos por nosotros y que cualquier usuario nuevo registrado sería considerado como usuario corriente sin permisos de administrador.

Manejo del modelo:

Para permitir sesiones de administrador y usuario se modifico la tabla usuario dentro de la base de datos, incorporando una columna con nombre "admin" la cual tiene un valor 1 en los usuarios que son administradores y un valor 0 en los usuarios corrientes.
La ventaja de esto es que se pudo modificar rápidamente la tabla usuarios en la base de datos para incorporar esta caracteristica, la desventaja presente es que hubo que llenar manualmente los valores de esta columna para cada usuario.

Modificaciones en la planilla:

Todas las modificaciones realizadas en los archivos de la planilla se encuentran comentados en los mismos archivos.

Dificultades y tiempo total empleado:

Como grupo no tuvimos mayores dificultades al momento de instalar las tecnologías ni analizando el enunciado, lo que más nos costo fue la implementación del DELETE puesto que se necesitaban considerar distintos casos dependiendo de cual usuario se iba a eliminar (el primero, el último o alguno de en medio). Es por esto mismo que lo que nos tomo más tiempo fue el desarrollo mismo de la plataforma para lograr cumplir con todos los requerimientos. 
A continuación, se describen tiempos promedios de desarrollo de la actividad:

Análisis del enunciado: En leer y comprender toda la actividad 15min.
Modificaciones y ajustes al modelo: En llegar a la idea e implementar la modificación planteada 1 hora.
Diseño de la plataforma: Se hicieron pocos cambios en temas de visualización por lo que el tiempo estimado es de 20min.
Desarrollo completo de la plataforma: Entre los errores del cambio y lograr aplicar todos los cambios realizados se invirtió alrededor de 45horas de trabajo en conjunto.

Tiempo total: 46 horas y 35 minutos aproximadamente.
